import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, RouterModule.forRoot([
    { path: 'sulley', loadChildren: ()=> import('@monsters.ink/tools-a').then(m => m.ToolsAModule)},
    { path: '', redirectTo: 'sulley', pathMatch: 'full' }
  ])],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
